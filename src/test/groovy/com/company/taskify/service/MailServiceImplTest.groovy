package com.company.taskify.service

import com.company.taskify.dao.entity.UserEntity
import com.company.taskify.model.request.TaskDto
import com.company.taskify.service.impl.MailServiceImpl
import org.apache.commons.mail.util.MimeMessageParser
import org.springframework.mail.javamail.JavaMailSender
import spock.lang.Specification
import javax.mail.Session
import javax.mail.internet.MimeMessage

class MailServiceImplTest extends Specification {

    private JavaMailSender javaMailSender = Mock()
    private MailService mailService;


    def "setup" () {
        mailService = new MailServiceImpl(javaMailSender)
    }

    def "sendMail success" () {
        given:
        UserEntity userEntity = UserEntity.builder()
                .surname("Suleymanov")
                .name("Zohrab")
                .id(1)
                .email("zohrab.suleymanov23@gmail.com").build()

        TaskDto taskDto = TaskDto.builder()
                .deadlineWithDay(5)
                .description("description")
                .title("title").build()

        Properties properties = System.getProperties()
        Session session = Session.getDefaultInstance(properties)
        MimeMessage message = new MimeMessage(session)

        when:
        mailService.sendMail(List.of(userEntity),taskDto, userEntity)

        then:
        1 * javaMailSender.createMimeMessage() >> message
        1 * javaMailSender.send(message)
        message.getSubject() == taskDto.title
        message.getHeader("To",null) == userEntity.email
        MimeMessageParser parser = new MimeMessageParser(message)
        parser.parse()
        String htmlContent = parser.getHtmlContent()
        htmlContent.contains("Reporter-Zohrab Suleymanov")
        htmlContent.contains("description")
        htmlContent.contains("deadline-5 day")
    }
}
