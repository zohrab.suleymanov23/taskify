package com.company.taskify.service

import com.company.taskify.config.security.jwt.JwtTokenUtil
import com.company.taskify.model.request.AuthRequestDto
import com.company.taskify.model.response.AuthResponseDto
import com.company.taskify.service.impl.AuthServiceImpl
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import spock.lang.Specification


class AuthServiceImplTest extends Specification {
    private UserDetailsService userDetailsService = Mock()
    private AuthenticationManager authManager = Mock()
    private JwtTokenUtil jwtTokenUtil = Mock()
    private AuthService authService

    def "setup" () {
        authService = new AuthServiceImpl(userDetailsService,authManager,jwtTokenUtil)
    }

    def "login success" () {
        given:
        AuthRequestDto authRequestDto = AuthRequestDto.builder()
                .username("zohrab.suleymanov23@gmail.com")
                .password("123456").build()

        def token  = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ6b2hyYWIyMw.KwoR0BWpkj-qt0uTTMHhfA9lbGu-H58Vh68HaVFtOUJzQAH7q4Y3vyXeeTdMuyeV4cs7sGedvhQRn0hMtTFqww"

        AuthResponseDto authResponseDto = AuthResponseDto.builder()
                .token(token)
                .build()

        UsernamePasswordAuthenticationToken tokenAuth = new UsernamePasswordAuthenticationToken(
                authRequestDto.getUsername(), authRequestDto.getPassword())

        UserDetails userDetails = Mock()

        Authentication authentication = new UsernamePasswordAuthenticationToken(authRequestDto.getUsername(),
                authRequestDto.getPassword(), userDetails.authorities)

        when:
        def actual = authService.login(authRequestDto)

        then:
        1 * authManager.authenticate(tokenAuth) >> authentication
        1 * userDetailsService.loadUserByUsername(authRequestDto.username) >> userDetails
        1 * jwtTokenUtil.generateToken(userDetails) >> token
        actual.token == authResponseDto.token
    }


    def "login error" () {
        given:
        AuthRequestDto authRequestDto = AuthRequestDto.builder()
                .username("zohrab.suleymanov23@gmail.com")
                .password("123456").build()

        def token  = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ6b2hyYWIyMw.KwoR0BWpkj-qt0uTTMHhfA9lbGu-H58Vh68HaVFtOUJzQAH7q4Y3vyXeeTdMuyeV4cs7sGedvhQRn0hMtTFqww"

        AuthResponseDto authResponseDto = AuthResponseDto.builder()
                .token(token)
                .build()

        UsernamePasswordAuthenticationToken tokenAuth = new UsernamePasswordAuthenticationToken(
                authRequestDto.getUsername(), authRequestDto.getPassword())

        UserDetails userDetails = Mock()

        Authentication authentication = new UsernamePasswordAuthenticationToken(authRequestDto.getUsername(),
                authRequestDto.getPassword())

        when:
        def actual = authService.login(authRequestDto)

        then:
        1 * authManager.authenticate(tokenAuth) >> authentication
        BadCredentialsException ex = thrown()
        ex.message == "Unknown username or password"
    }
}
