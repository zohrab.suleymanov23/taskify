package com.company.taskify.model.response;

import com.company.taskify.model.request.OrganizationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserView {
    private String name;
    private String surname;
    private OrganizationDto organization;
}
