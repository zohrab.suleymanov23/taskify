package com.company.taskify.model.enums;

public enum TaskStatus {
    TODO,
    IN_PROGRESS,
    REVIEW,
    DONE,
    CLOSED
}
