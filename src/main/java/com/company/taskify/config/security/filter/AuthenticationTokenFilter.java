package com.company.taskify.config.security.filter;

import com.company.taskify.config.security.jwt.JwtTokenUtil;
import com.company.taskify.exception.AuthException;
import com.company.taskify.exception.InvalidHeaderException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.company.taskify.model.enums.ExceptionCode.ACCESS_DENIED;
import static com.company.taskify.model.enums.ExceptionCode.INVALID_HEADER;

public class AuthenticationTokenFilter extends OncePerRequestFilter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	@Qualifier("appUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	@Qualifier("handlerExceptionResolver")
	private HandlerExceptionResolver resolver;

	private final String tokenHeader = "Authorization";

	private static final String[] AUTH_WHITELIST = {
			"v2/api-docs",
			"swagger-resources",
			"swagger-resources",
			"configuration/ui",
			"configuration/security",
			"swagger-ui.html",
			"webjars",
			"v3/api-docs",
			"swagger-ui",
			"auth",
			"sign-up",
			"console",
	};

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		
		final String requestHeader = request.getHeader(this.tokenHeader);

		String username, authToken;
		boolean isPermit = false;

		try {

			 for (String s : AUTH_WHITELIST) {
				 if (request.getServletPath().contains(s)) {
					 isPermit = true;
					 break;
				 }
			 }

			 if (!isPermit) {

				 if ((requestHeader != null)) {
					 authToken = requestHeader;
					 try {
						 username = jwtTokenUtil.getUsernameFromToken(authToken);
					 } catch (MalformedJwtException | ExpiredJwtException e) {
						 logger.error("an error occurred during getting username from token", e);
						 throw new AuthException(ACCESS_DENIED.getMessage(), ACCESS_DENIED.getCode());
					 }
				 } else {
					 throw new InvalidHeaderException(INVALID_HEADER.getMessage(), INVALID_HEADER.getCode());
				 }

				 checkAuthentication(username, authToken, request);
			 }
			 chain.doFilter(request,response);

		 }
		 catch (RuntimeException ex) {
			 resolver.resolveException(request, response, null, ex);
		 }
	}

	private void checkAuthentication(String username, String authToken, HttpServletRequest request) {
		logger.info(" checking authentication for user {} ", username);
		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

			UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

			if (jwtTokenUtil.validateToken(authToken, userDetails)) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				logger.info("authenticated user " + username + ", setting security context");
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}
	}

}