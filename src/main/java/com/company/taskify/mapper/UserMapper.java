package com.company.taskify.mapper;

import com.company.taskify.dao.entity.UserEntity;
import com.company.taskify.model.request.SignUpDto;
import com.company.taskify.model.request.UserDto;
import com.company.taskify.model.response.UserView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = OrganizationMapper.class)
public abstract class UserMapper {
    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "organizationEntity",source = "signUpDto.organization")
    public abstract UserEntity signUpDtoToEntity(SignUpDto signUpDto);

    public abstract UserEntity userDtoToEntity(UserDto userDto);

    @Mapping(target = "organization",source = "userEntity.organizationEntity")
    public abstract UserView mapView(UserEntity userEntity);

    public abstract List<UserView> mapViews(List<UserEntity>userEntities);
}
