package com.company.taskify.service;

import com.company.taskify.dao.entity.UserEntity;
import com.company.taskify.model.request.TaskDto;

import java.util.List;

public interface MailService {
    void sendMail(List<UserEntity> userList, TaskDto taskDto, UserEntity reporter);
}
