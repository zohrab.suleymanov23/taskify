package com.company.taskify.service.impl;

import com.company.taskify.dao.entity.OrganizationEntity;
import com.company.taskify.dao.entity.RoleEntity;
import com.company.taskify.dao.entity.UserEntity;
import com.company.taskify.dao.repository.OrganizationRepository;
import com.company.taskify.dao.repository.RoleRepository;
import com.company.taskify.dao.repository.UserRepository;
import com.company.taskify.exception.OrganizationNotFoundException;
import com.company.taskify.exception.OrganizationRegisteredException;
import com.company.taskify.exception.RoleNotFoundException;
import com.company.taskify.exception.UserRegisteredException;
import com.company.taskify.mapper.UserMapper;
import com.company.taskify.model.enums.RoleName;
import com.company.taskify.model.request.SignUpDto;
import com.company.taskify.model.request.UserDto;
import com.company.taskify.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Optional;
import static com.company.taskify.model.enums.ExceptionCode.ORGANIZATION_ALREADY_REGISTERED;
import static com.company.taskify.model.enums.ExceptionCode.ORGANIZATION_NOT_FOUND;
import static com.company.taskify.model.enums.ExceptionCode.ROLE_NOT_FOUND;
import static com.company.taskify.model.enums.ExceptionCode.USER_ALREADY_REGISTERED;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private OrganizationRepository organizationRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    public UserServiceImpl(UserRepository userRepository,
                           OrganizationRepository organizationRepository,
                           RoleRepository roleRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.organizationRepository = organizationRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void createOrganization(SignUpDto signUpDto) {
        logger.info("ActionLog.createOrganization.start");
        //1. check user
        Optional<UserEntity> user =  userRepository.findByEmail(signUpDto.getEmail());
        if (user.isPresent()) {
           logger.error("ActionLog.createOrganization.error --user already registered");
           throw new UserRegisteredException(USER_ALREADY_REGISTERED.getMessage(), USER_ALREADY_REGISTERED.getCode());
        }

        //2. check organization
        Optional<OrganizationEntity> organization = organizationRepository
               .findByOrganizationName(signUpDto.getOrganization().getOrganizationName());

        if (organization.isPresent()) {
           logger.error("ActionLog.createOrganization.error --organization already registered");
           throw new OrganizationRegisteredException(ORGANIZATION_ALREADY_REGISTERED.getMessage(),
                   ORGANIZATION_ALREADY_REGISTERED.getCode());
        }

        //3.check role
        RoleEntity role = roleRepository.findByRoleName(RoleName.ADMIN.getRoleName())
                .orElseThrow(()->{
                    logger.error("ActionLog.createOrganization.error --role {} not found",RoleName.ADMIN.getRoleName());
                    throw new RoleNotFoundException(ROLE_NOT_FOUND.getMessage(), ROLE_NOT_FOUND.getCode());
                });
        UserEntity userEntity = UserMapper.INSTANCE.signUpDtoToEntity(signUpDto);
        userEntity.setRole(role);
        userEntity.setPassword(bCryptPasswordEncoder.encode(signUpDto.getPassword()));
        userRepository.save(userEntity);

        logger.info("ActionLog.createOrganization.end");
    }

    @Override
    public void addUser(UserDto userDto, String username) {
        logger.info("ActionLog.addUser.start");
        //1. check user
        Optional<UserEntity> user= userRepository.findByEmail(userDto.getEmail());
        if (user.isPresent()) {
            logger.error("ActionLog.addUser.error --user already registered");
            throw new UserRegisteredException(USER_ALREADY_REGISTERED.getMessage(),
                    USER_ALREADY_REGISTERED.getCode());
        }

        //2 check organization
        OrganizationEntity organizationEntity = userRepository.findByEmail(username)
                .orElseThrow(()->{
                    logger.error("ActionLog.addUser.error --unexpected error");
                    throw new IllegalStateException("unexpected error");
                })
                .getOrganizationEntity();
        if (organizationEntity == null) {
            logger.error("ActionLog.addUser.error --organization not found");
            throw new OrganizationNotFoundException(ORGANIZATION_NOT_FOUND.getMessage(),
                    ORGANIZATION_NOT_FOUND.getCode());
        }

        //3.check role
        RoleEntity role = roleRepository.findByRoleName(RoleName.USER.getRoleName())
                .orElseThrow(()-> {
                    logger.error("ActionLog.createOrganization.error --role {} not found",RoleName.USER.getRoleName());
                    throw new RoleNotFoundException(ROLE_NOT_FOUND.getMessage(), ROLE_NOT_FOUND.getCode());
                });

        UserEntity userEntity = UserMapper.INSTANCE.userDtoToEntity(userDto);
        userEntity.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        userEntity.setRole(role);
        userEntity.setOrganizationEntity(organizationEntity);
        logger.info("ActionLog.addUser.end");
        userRepository.save(userEntity);
    }
}
