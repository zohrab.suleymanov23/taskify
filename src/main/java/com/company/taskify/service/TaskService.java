package com.company.taskify.service;

import com.company.taskify.model.request.TaskDto;
import com.company.taskify.model.response.TaskView;

import java.util.List;

public interface TaskService {
   void createTask(TaskDto taskDto, String username);
   List<TaskView> getAllOrganizationTask(String username);
}
