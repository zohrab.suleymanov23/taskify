package com.company.taskify.service;

import com.company.taskify.model.request.AuthRequestDto;
import com.company.taskify.model.response.AuthResponseDto;

public interface AuthService {
    AuthResponseDto login(AuthRequestDto authRequestDto);
}
