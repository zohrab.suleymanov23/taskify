package com.company.taskify.service;

import com.company.taskify.model.request.SignUpDto;
import com.company.taskify.model.request.UserDto;

public interface UserService {
    void createOrganization(SignUpDto signUpDto);
    void addUser(UserDto userDto, String username);
}
