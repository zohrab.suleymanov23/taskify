package com.company.taskify.dao.repository;

import com.company.taskify.dao.entity.OrganizationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrganizationRepository extends CrudRepository<OrganizationEntity, Long> {
    Optional<OrganizationEntity> findByOrganizationName(String organizationName);
}
