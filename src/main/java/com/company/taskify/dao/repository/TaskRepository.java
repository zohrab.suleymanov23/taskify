package com.company.taskify.dao.repository;

import com.company.taskify.dao.entity.OrganizationEntity;
import com.company.taskify.dao.entity.TaskEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<TaskEntity, Long> {
    List<TaskEntity> findByOrganization(OrganizationEntity organizationEntity);
}
