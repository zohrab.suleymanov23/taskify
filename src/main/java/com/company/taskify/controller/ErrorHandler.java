package com.company.taskify.controller;

import com.company.taskify.exception.*;
import com.company.taskify.model.response.ExceptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(ErrorHandler.class);


    @ExceptionHandler(InvalidHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleInvalidHeaderException(InvalidHeaderException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode());
    }

    @ExceptionHandler(AuthException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ExceptionResponse handleAuthException(AuthException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode());
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleUserNotFoundException(UserNotFoundException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode());
    }

    @ExceptionHandler(OrganizationNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleOrganizationNotFoundException(OrganizationNotFoundException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode());
    }

    @ExceptionHandler(OrganizationRegisteredException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse handleUserNotFoundException(OrganizationRegisteredException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode());
    }


    @ExceptionHandler(RoleNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleRoleNotFoundException(RoleNotFoundException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode());
    }

    @ExceptionHandler(UserRegisteredException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse handleOrganizationNotFoundException(UserRegisteredException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode());
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ExceptionResponse handleBadCredentialsException(BadCredentialsException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                "Bad credentials");
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse handle(Exception ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
                "UNEXPECTED_EXCEPTION");
    }

}
